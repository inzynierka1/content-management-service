#!/usr/bin/env bash

docker run -d -p 27018:27017 mongo
docker run -d -p 27017:27017 mongo
docker rm auth-management
docker rm content-management
docker run -d --name=auth-management -p 8080:8080 \
    -e ME_ENGINEER_MONGO_AUTH=docker.for.mac.localhost:27017 \
    -e ME_ENGINEER_MONGO_DB_NAME_AUTH=NetworkDB \
    -e ME_ENGINEER_CONTENT_MANAGEMENT_ADDRESS_AUTH=http://docker.for.mac.localhost:8081/api \
    bartekf95/auth-management-service
docker run -d --name=content-management -p 8081:8081 \
    -e ME_ENGINEER_MONGO_CONTENT=docker.for.mac.localhost:27018 \
    -e ME_ENGINEER_IMAGES_STORAGE_PATH=/usr/storage \
    bartekf95/content-management-service
