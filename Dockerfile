FROM java

ADD content-management-service-0.0.2.jar /app
RUN mkdir /usr/storage
ENTRYPOINT [ "sh", "-c", "java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -jar /app" ]