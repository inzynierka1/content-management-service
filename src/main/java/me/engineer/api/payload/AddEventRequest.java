package me.engineer.api.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.engineer.domain.Event;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddEventRequest {

    @NotNull
    private String userId;
    @NotNull
    private String title;
    @NotNull
    private String content;
    private List<ImageRequest> images = Collections.emptyList();
    private List<String> groups = Collections.emptyList();


    public Event toEvent() {
        return new Event(
                title,
                userId,
                content,
                Optional.ofNullable(images).orElseGet(Collections::emptyList)
                        .stream().map(ImageRequest::toImage).collect(Collectors.toList()),
                groups);
    }
}
