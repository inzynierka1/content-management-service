package me.engineer.api.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.engineer.domain.Event;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventResponse {
    private String id;
    private String userId;
    private String title;
    private String content;
    private List<ImageRequest> images;
    private List<String> groups;
    private String timestamp;


    public static EventResponse fromEvent(Event event) {
        return new EventResponse(
                event.getId(),
                event.getUserId(),
                event.getTitle(),
                event.getContent(),
                event.getImages().stream().map(ImageRequest::fromImage).collect(Collectors.toList()),
                event.getGroups(),
                event.getTimestamp().toString());
    }
}
