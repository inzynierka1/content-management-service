package me.engineer.api.payload;

import me.engineer.domain.Image;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
public class ImageRequest {
    private String id;
    private String name;

    public ImageRequest() {}

    public ImageRequest(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Image toImage() {
        return new Image(id, name);
    }

    public static ImageRequest fromImage(Image image) {
        return new ImageRequest(image.getId(), image.getName());
    }

    public String getId() {
        return id;
    }


    public String getName() {
        return name;
    }

}
