package me.engineer.api;

import me.engineer.api.payload.AddEventRequest;
import me.engineer.api.payload.EventResponse;
import me.engineer.domain.Event;
import me.engineer.exceptions.EventNotFoundException;
import me.engineer.service.impl.EventServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
@RestController
public class EventController {

    @Autowired
    private EventServiceImpl eventService;

    @GetMapping("api/events")
    @ResponseStatus(HttpStatus.OK)
    public List<EventResponse> getAllEvents() {
        return eventService.getAllEvents()
                .stream()
                .map(EventResponse::fromEvent)
                .collect(Collectors.toList());
    }

    @GetMapping("api/events/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public List<EventResponse> getEventsForUser(@PathVariable("userId") String userId) {
        return eventService.getAllEventsForUser(userId)
                .stream()
                .map(EventResponse::fromEvent)
                .collect(Collectors.toList());

    }

    @GetMapping("api/events/{eventId}")
    @ResponseStatus(HttpStatus.OK)
    public EventResponse getEvent(@PathVariable("eventId") String eventId) {
        return eventService.getEventById(eventId)
                .map(EventResponse::fromEvent)
                .orElseThrow(EventNotFoundException::new);
    }

    @PutMapping("api/events/{eventId}")
    @ResponseStatus(HttpStatus.OK)
    public EventResponse updateEvent(@PathVariable("eventId") String eventId, @RequestBody @Valid AddEventRequest request) {
        Event eventFromRequest = request.toEvent();
        eventFromRequest.setId(eventId);
        Event editedEvent = eventService.saveEvent(eventFromRequest);
        return EventResponse.fromEvent(editedEvent);
    }

    @DeleteMapping("api/events/{eventId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEvent(@PathVariable("eventId") String eventId) {
        eventService.deleteEvent(eventId);
    }

    @GetMapping("api/events/user/{userId}/{groups}")
    @ResponseStatus(HttpStatus.OK)
    public List<EventResponse> getEventsForGroups(@PathVariable("userId") String userId,
                                                  @PathVariable("groups") String groups) {
        List<String> groupIds = Arrays.asList(groups.split(","));
        List<Event> newOne = eventService.getAllEventsForUserWithGroups(userId, groupIds);
        return newOne.stream()
                .map(EventResponse::fromEvent).collect(Collectors.toList());
    }

//    @GetMapping("api/events/user/{userId}/{groups}/search")
//    @ResponseStatus(HttpStatus.OK)
//    public List<EventResponse> getEvents(@PathVariable("userId") String userId,
//                                         @PathVariable("groups") String groups,
//                                         @RequestParam("title") String title) {
//
//
//        eventService.find();
//        return Collections.emptyList();
//    }

    @PostMapping("api/events")
    @ResponseStatus(HttpStatus.CREATED)
    public EventResponse addEvent(@RequestBody @Valid AddEventRequest request) {
        Event addedEvent = eventService.saveEvent(request.toEvent());
        return EventResponse.fromEvent(addedEvent);
    }
}
