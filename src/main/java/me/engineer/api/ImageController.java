package me.engineer.api;

import me.engineer.domain.Event;
import me.engineer.domain.Image;
import me.engineer.exceptions.EventNotFoundException;
import me.engineer.exceptions.ImageNotFoundException;
import me.engineer.exceptions.ImageSaveFiledException;
import me.engineer.service.EventService;
import me.engineer.service.ImageService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Bartosz Frankowski on 24.09.2017
 */
@RestController
public class ImageController {

    @Autowired
    private EventService eventService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private HttpServletRequest request;

    @PostMapping(value = "api/images/{userId}/{eventId}", headers = "content-type=multipart/*")
    @ResponseStatus(HttpStatus.OK)
    public void saveImage(@PathVariable String eventId,
                          @RequestParam MultipartFile file) throws IOException {

        Event requestedEvent = eventService.getEventById(eventId)
                .orElseThrow(EventNotFoundException::new);

        Image image = imageService.addImageToEvent(requestedEvent, file)
                .orElseThrow(ImageSaveFiledException::new);

        requestedEvent.getImages().add(image);
        eventService.saveEvent(requestedEvent);
    }

    @GetMapping(value = "api/images/{userId}/{eventId}/{imageId}")
    @ResponseStatus(HttpStatus.OK)
    public void getImage(@PathVariable String userId, @PathVariable String eventId,
                         @PathVariable String imageId, HttpServletResponse response) throws IOException {

        Optional<File> foundImage = imageService.getImageFor(userId, eventId, imageId);
        if (foundImage.isPresent()) {
            sendImageToResponse(response, foundImage.get());
        } else {
            removeMissingImage(eventId, imageId);
            throw new ImageNotFoundException();
        }

    }

    private int sendImageToResponse(HttpServletResponse response, File destFile) throws IOException {
        return IOUtils.copy(new FileInputStream(destFile), response.getOutputStream());
    }

    private void removeMissingImage(@PathVariable String eventId, @PathVariable String imageId) {
        Event event = eventService.getEventById(eventId).orElseThrow(EventNotFoundException::new);
        List<Image> imagesWithRemovedMissing = event.getImages().stream()
                .filter(image -> !image.getId().equals(imageId))
                .collect(Collectors.toList());
        event.setImages(imagesWithRemovedMissing);
        eventService.saveEvent(event);
    }
}