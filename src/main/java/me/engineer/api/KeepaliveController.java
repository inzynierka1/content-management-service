package me.engineer.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Bartosz Frankowski on 24.09.2017
 */
@RestController
public class KeepaliveController {

    @GetMapping("api/test")
    @ResponseStatus(HttpStatus.OK)
    public StringResponse isServiceAlive() {
        return new StringResponse("OK");
    }

    private static class StringResponse {
        private String value;

        public StringResponse(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
