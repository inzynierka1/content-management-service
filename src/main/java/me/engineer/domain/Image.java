package me.engineer.domain;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
public class Image {
    //Just a placeholder for now
    private String id;
    private String name;

    public Image(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
