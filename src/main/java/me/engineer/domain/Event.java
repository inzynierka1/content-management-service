package me.engineer.domain;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */

@Data
public class Event {
    private String id;
    private String userId;
    private String title;
    private String content;
    private List<Image> images;
    private List<String> groups;
    private LocalDateTime timestamp;

    public Event(String id, String userId , String title, String content, List<Image> images,
                 List<String> groups,LocalDateTime timestamp) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.content = content;
        this.images = images;
        this.groups = groups;
        this.timestamp = timestamp;
    }

    public Event(String userId, String title, String content, List<Image> images, List<String> groups) {
        this("",title,userId,content,images,groups, LocalDateTime.now());
    }
}
