package me.engineer;

import lombok.extern.slf4j.Slf4j;
import me.engineer.config.API;
import me.engineer.config.Storage;
import me.engineer.config.providers.ConfigFromEnvProvider;
import me.engineer.config.providers.ConfigProvider;
import me.engineer.service.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("me.engineer")
@Slf4j
public class ContentManagementService extends SpringApplication{


    public static void main(String[] args) {
        SpringApplication.run(ContentManagementService.class);
    }

    @Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
            storageService.init();
        };
    }

    @Bean
    API initApi() {
        ConfigProvider apiProvider = new ConfigFromEnvProvider();
        return apiProvider.getApiConfig();
    }

    @Bean
    Storage initStorage() {
        ConfigProvider apiProvider = new ConfigFromEnvProvider();
        return apiProvider.getStorageConfig();
    }
}
