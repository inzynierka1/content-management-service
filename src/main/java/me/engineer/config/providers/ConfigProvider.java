package me.engineer.config.providers;

import me.engineer.config.API;
import me.engineer.config.Storage;

/**
 * Created by Bartosz Frankowski on 10.10.2017
 */
public interface ConfigProvider {

    API getApiConfig();

    Storage getStorageConfig();
}
