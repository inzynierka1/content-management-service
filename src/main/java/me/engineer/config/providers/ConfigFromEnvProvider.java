package me.engineer.config.providers;

import lombok.extern.slf4j.Slf4j;
import me.engineer.config.API;
import me.engineer.config.Storage;

import java.util.Map;

/**
 * Created by Bartosz Frankowski on 10.10.2017
 */
@Slf4j
public class ConfigFromEnvProvider implements ConfigProvider {
    private static final String ENV_MONGO_CONTENT = "ME_ENGINEER_MONGO_CONTENT";
    private static final String DEFAULT_MONGO_ADDRESS = "localhost:27018";
    private static final String ENV_IMAGES_STORAGE_PATH = "ME_ENGINEER_IMAGES_STORAGE_PATH";
    private Map<String, String> environmentalVariables;

    public ConfigFromEnvProvider() {
        log.debug("Trying to obtain API info");
        environmentalVariables = System.getenv();
    }

    @Override
    public API getApiConfig() {
        if (environmentalVariables.containsKey(ENV_MONGO_CONTENT)) {
            String mongoAddress = environmentalVariables.get(ENV_MONGO_CONTENT);
            log.info("MongoDB address obtained: {}", mongoAddress);
            return new API(mongoAddress);
        }
        log.warn("Env {} not found! Using default MongoDB address: {} ", ENV_MONGO_CONTENT, DEFAULT_MONGO_ADDRESS);
        return new API(DEFAULT_MONGO_ADDRESS);
    }

    @Override
    public Storage getStorageConfig() {
        if (environmentalVariables.containsKey(ENV_IMAGES_STORAGE_PATH)) {
            String imagesStoragePath = environmentalVariables.get(ENV_IMAGES_STORAGE_PATH);
            log.info("Storage obtained for images: {}", imagesStoragePath);
            return new Storage(imagesStoragePath);
        }
        log.warn("Env {} not found! Using context images storage", ENV_IMAGES_STORAGE_PATH);
        return new Storage();
    }
}
