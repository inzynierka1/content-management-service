package me.engineer.config;

import lombok.Getter;

/**
 * Created by Bartosz Frankowski on 10.10.2017
 */

public class API {
    @Getter
    private String mongoAddress;

    public API(String mongoAddress) {

        this.mongoAddress = mongoAddress;
    }
}
