package me.engineer.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
@Configuration
@EnableMongoRepositories("me.engineer")
public class MongoConfig extends AbstractMongoConfiguration{

    private static final String DB_NAME = "content-db";

    @Autowired
    private API api;

    @Override
    protected String getDatabaseName() {
        return DB_NAME;
    }

    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient(api.getMongoAddress());
    }
}
