package me.engineer.config;

import lombok.NonNull;

import java.util.Optional;

/**
 * Created by Bartosz Frankowski on 16.10.2017
 */
public class Storage {
    private String imagesStoragePath;

    public Storage(@NonNull String imagesStoragePath) {
        this.imagesStoragePath = imagesStoragePath;
    }

    public Storage() {
    }

    public Optional<String> getImagesStoragePath() {
        return Optional.ofNullable(imagesStoragePath);
    }
}
