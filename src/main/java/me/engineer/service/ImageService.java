package me.engineer.service;

import me.engineer.domain.Event;
import me.engineer.domain.Image;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Optional;

/**
 * Created by Bartosz Frankowski on 07.10.2017
 */
public interface ImageService {

    Optional<Image> addImageToEvent(Event event, MultipartFile multipartFile);

    Optional<File> getImageFor(String userId, String eventId, String imageId);
}
