package me.engineer.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Created by Bartosz Frankowski on 24.09.2017
 */
public interface StorageService {

    void init();

    String store(String path, String imageName, MultipartFile file) throws IOException;

    Path load(String filename);

    void delete();
}
