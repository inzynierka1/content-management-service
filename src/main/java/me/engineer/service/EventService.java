package me.engineer.service;

import me.engineer.domain.Event;

import java.util.List;
import java.util.Optional;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
public interface EventService {
    List<Event> getAllEvents();
    List<Event> getAllEventsForUser(String userId);

    List<Event> getAllEventsForUserWithGroups(String userId, List<String> groupIds);

    Optional<Event> getEventById(String id);

    Event saveEvent(Event event);

    void deleteEvent(String eventId);
}
