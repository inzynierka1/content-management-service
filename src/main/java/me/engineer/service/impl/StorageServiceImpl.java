package me.engineer.service.impl;

import lombok.extern.slf4j.Slf4j;
import me.engineer.service.StorageService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Created by Bartosz Frankowski on 24.09.2017
 */
@Slf4j
@Service
public class StorageServiceImpl implements StorageService {

    @Override
    public void init() {

    }

    @Override
    public String store(String path, String imageName, MultipartFile file) throws IOException {

        ObjectId imageId = ObjectId.get();
        String imageFileName = imageName + "-" + imageId.toString();
        File file1 = new File(path);
        file1.mkdir();
        File destFile = new File(path + "/" + imageFileName);
        destFile.createNewFile();
        log.debug("Writing file {} to path: {}", imageName, destFile.getAbsolutePath());
        file.transferTo(destFile);
        return imageId.toString();
    }

    @Override
    public Path load(String filename) {
        return null;
    }

    @Override
    public void delete() {

    }
}
