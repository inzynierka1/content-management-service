package me.engineer.service.impl;

import me.engineer.domain.Event;
import me.engineer.persistence.EventRepositoryAdapter;
import me.engineer.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepositoryAdapter eventRepository;

    @Override
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @Override
    public List<Event> getAllEventsForUser(String userId) {
        return eventRepository.findByUserId(userId);
    }

    @Override
    public List<Event> getAllEventsForUserWithGroups(String userId, List<String> groupIds) {
        return eventRepository.findAllByUserIdAndGroupIds(userId, groupIds);
    }

    @Override
    public Optional<Event> getEventById(String id) {
        return eventRepository.findByEventId(id);
    }

    @Override
    public Event saveEvent(Event event) {
        return eventRepository.save(event);
    }

    @Override
    public void deleteEvent(String eventId) {
        eventRepository.deleteEvent(eventId);
    }
}
