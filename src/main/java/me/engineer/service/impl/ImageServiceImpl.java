package me.engineer.service.impl;

import lombok.extern.slf4j.Slf4j;
import me.engineer.config.Storage;
import me.engineer.domain.Event;
import me.engineer.domain.Image;
import me.engineer.service.ImageService;
import me.engineer.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by Bartosz Frankowski on 07.10.2017
 */
@Service
@Slf4j
public class ImageServiceImpl implements ImageService {

    private final ServletContext context;

    private final Storage storage;

    private final StorageService storageService;

    @Autowired
    public ImageServiceImpl(ServletContext context, Storage storage, StorageService storageService) {
        this.context = context;
        this.storage = storage;
        this.storageService = storageService;
    }

    @Override
    public Optional<Image> addImageToEvent(Event event, MultipartFile multipartFile) {
        String path = storage.getImagesStoragePath().orElse(context.getRealPath(""));
        try {
            log.debug("Trying to save image for event {} ", event.getId());
            String imageId = storageService.store(path + "/" + event.getUserId(), event.getId(), multipartFile);
            log.debug("Image saved with id {} ", imageId);
            return Optional.of(new Image(imageId, "image-name"));
        } catch (IOException e) {
            log.error("Exception while saving image {} " + e.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public Optional<File> getImageFor(String userId, String eventId, String imageId) {
        String imagePath = constructImagePath(userId, eventId, imageId);
        File imageFile = new File(imagePath);

        if (imageFile.exists()) {
            log.debug("Image found: {}", imagePath);
            return Optional.of(imageFile);
        }
        log.error("No such image: {}", imagePath);
        return Optional.empty();
    }

    private String constructImagePath(String userId, String eventId, String imageId) {
        String basePath = storage.getImagesStoragePath().orElse(context.getRealPath(""));
        return basePath + "/" + userId + "/" + eventId + "-" + imageId ;
    }
}
