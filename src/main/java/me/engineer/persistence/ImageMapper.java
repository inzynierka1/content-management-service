package me.engineer.persistence;

import me.engineer.domain.Image;
import org.bson.types.ObjectId;

/**
 * Created by Bartosz Frankowski on 18.07.2017
 */
class ImageMapper {
    static ImageEntity map(Image image) {
        return new ImageEntity(new ObjectId(image.getId()), image.getName());
    }

    static Image map(ImageEntity imageEntity) {
        return new Image(imageEntity.getId().toHexString(), imageEntity.getName());
    }
}
