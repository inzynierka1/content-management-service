package me.engineer.persistence;

import me.engineer.domain.Image;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
class ImageEntity {
    @Id
    private ObjectId id;
    private String name;

    ImageEntity(ObjectId id, String name) {
        this.id = id;
        this.name = name;
    }

    Image toImage() {
        return new Image(id.toHexString(), name);
    }

    ObjectId getId() {
        return id;
    }

    String getName() {
        return name;
    }
}
