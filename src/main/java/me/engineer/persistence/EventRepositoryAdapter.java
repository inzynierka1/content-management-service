package me.engineer.persistence;

import me.engineer.domain.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Bartosz Frankowski on 18.07.2017
 */
@Component
public class EventRepositoryAdapter {


    @Autowired
    private EventRepository eventRepository;

    public List<Event> findAll() {
        return eventRepository.findAll().stream().map(EventMapper::map).collect(Collectors.toList());
    }

    public List<Event> findByUserId(String userId) {
        return eventRepository.getAllByUserId(userId).map(EventMapper::map).collect(Collectors.toList());
    }

    public Optional<Event> findByEventId(String id) {
        return eventRepository.getAllById(id).map(EventMapper::map).findFirst();
    }

    public Event save(Event event) {
        EventEntity savedEntity = eventRepository.save(EventMapper.map(event));
        return EventMapper.map(savedEntity);
    }

    public List<Event> findAllByGroupIds(List<String> groupIds) {
        return eventRepository.getAllByGroupsIn(groupIds)
                .map(EventMapper::map)
                .collect(Collectors.toList());
    }

    public List<Event> findAllByUserIdAndGroupIds(String userId, List<String> groupIds) {
        return eventRepository.getAllByUserIdOrGroupsInOrderByTimestampDesc(userId, groupIds)
                .map(EventMapper::map)
                .collect(Collectors.toList());
    }

    public void deleteEvent(String eventId) {
        eventRepository.delete(eventId);
    }
}
