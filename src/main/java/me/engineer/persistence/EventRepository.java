package me.engineer.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
@Repository
interface EventRepository extends MongoRepository<EventEntity, String> {
    Stream<EventEntity> getAllByUserId(String userId);
    Stream<EventEntity> getAllById(String id);
    Stream<EventEntity> getAllByGroupsIn(List<String> groups);
    Stream<EventEntity> getAllByGroupsInOrUserIdOrderByTimestampDesc(String userId, List<String> groups);
    Stream<EventEntity> getAllByUserIdOrGroupsInOrderByTimestampDesc(String userId, List<String> groups);
    Stream<EventEntity> getAllByTitleStartsWithAndGroupsIn(String title, List<String> groups);
}
