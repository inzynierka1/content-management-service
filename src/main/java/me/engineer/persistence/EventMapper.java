package me.engineer.persistence;

import me.engineer.domain.Event;
import org.bson.types.ObjectId;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

/**
 * Created by Bartosz Frankowski on 06.07.2017
 */
class EventMapper {

    static EventEntity map(Event event) {
        ObjectId id = event.getId() == null || event.getId().isEmpty() ? ObjectId.get() : new ObjectId(event.getId());

        return EventEntity.builder()
                .id(id)
                .title(event.getTitle())
                .content(event.getContent())
                .userId(event.getUserId())
                .timestamp(event.getTimestamp().toString())
                .images(event.getImages().stream().map(ImageMapper::map).collect(Collectors.toList()))
                .groups(event.getGroups())
                .build();
    }

    static Event map(EventEntity eventEntity) {
        return new Event(
                eventEntity.getId().toHexString(),
                eventEntity.getUserId(),
                eventEntity.getTitle(),
                eventEntity.getContent(),
                eventEntity.getImages().stream().map(ImageMapper::map).collect(Collectors.toList()),
                eventEntity.getGroups(),
                str2Date(eventEntity.getTimestamp())
        );
    }

    private static LocalDateTime str2Date(String string) {
        return LocalDateTime.parse(string, DateTimeFormatter.ISO_DATE_TIME);
    }
}
