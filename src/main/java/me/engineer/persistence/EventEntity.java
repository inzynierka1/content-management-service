package me.engineer.persistence;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.List;

/**
 * Created by Bartosz Frankowski on 02.07.2017
 */
@Builder
@Data
class EventEntity {

    @Id
    private ObjectId id;
    private String userId;
    private String title;
    private String content;
    private List<ImageEntity> images;
    private List<String> groups;
    private String timestamp;
}
