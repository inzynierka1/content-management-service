#How to run?
* `./gradlew buildDocker (if you want to build new version)`
* `docker run -d -p 27018:27017 mongo`
* `docker run -d -p 8081:8081 bartekf95/content-management-service`

###### 2 lower commands are enough to just run this service (it will be pulled from docker hub)

##To enable run from intellij:
#####Add to /etc/host
`127.0.0.1       docker.for.mac.localhost`